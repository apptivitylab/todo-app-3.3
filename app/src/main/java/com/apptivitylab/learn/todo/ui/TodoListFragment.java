package com.apptivitylab.learn.todo.ui;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apptivitylab.learn.todo.AddTaskActivity;
import com.apptivitylab.learn.todo.R;

/**
 * TodoList shows a list of items to remind.
 */
public class TodoListFragment extends Fragment {
    public static final String TAG = "TodoListFragment";

    private FloatingActionButton mAddTaskFab;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_todo_list, container, false);

        mAddTaskFab = (FloatingActionButton) rootView.findViewById(R.id.fab);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAddTaskFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddTaskActivity();
            }
        });
    }

    private void showAddTaskFragment() {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_vg_container, new AddTaskFragment())
                .addToBackStack(AddTaskFragment.TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }

    private void showAddTaskActivity() {
        Snackbar.make(getView(), "Show Add Task as Activity", Snackbar.LENGTH_SHORT).show();

        Intent launchIntent = new Intent(getContext(), AddTaskActivity.class);
        startActivity(launchIntent);
    }
}
